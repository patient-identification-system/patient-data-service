<?php

namespace Database\Seeders;

use Database\Factories\FakeDataFactoryHelpers;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class MedicalRecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        $treatmentsAndDiagnoses = FakeDataFactoryHelpers::getDiagnosesAndTreatments();

        for($i = 0; $i <= 19; $i++) {
            // get a random number for the diagnosis
            $randomNumberDiagnosis = array_rand($treatmentsAndDiagnoses);

            DB::table('medical_records')->insert([
                'patient_id' => rand(1, 20),
                'record_date' => Carbon::now()->format('Y-m-d H:i:s'),
                'diagnosis' => $randomNumberDiagnosis,
                'treatment' => $treatmentsAndDiagnoses[$randomNumberDiagnosis],
                'doctor_id' => rand(1, 5),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
