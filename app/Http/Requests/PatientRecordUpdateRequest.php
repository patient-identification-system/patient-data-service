<?php

namespace App\Http\Requests;

use App\Validation\GenderRule;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class PatientRecordUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => 'max:255',
            'last_name' => 'max:255',
            'dob' => 'date',
            'gender' => [new GenderRule()],
            'contact_number' => 'max:25',
            'address' => 'max:255'
        ];
    }
}
