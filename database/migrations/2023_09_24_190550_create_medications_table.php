<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * CREATE TABLE medications (
         * medication_id INT PRIMARY KEY,
         * medication_name VARCHAR(100),
         * dosage VARCHAR(50)
     * );
     */
    public function up(): void
    {
        Schema::create('medications', function (Blueprint $table) {
            $table->bigIncrements('medication_id');
            $table->string('medication_name', 100);
            $table->string('dosage', 50);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('medications');
    }
};
