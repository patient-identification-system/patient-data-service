<?php

namespace App\Http\Controllers\api;

use App\Helpers\ValidationHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\PatientRecordCreateRequest;
use App\Http\Requests\PatientRecordUpdateRequest;
use App\Models\Patient;
use Illuminate\Http\JsonResponse;

class PatientInformationController extends Controller
{
    /**
     * Display the specified resource.
     *
     * The API is meant to return patients one at a time only.
     *
     * 200 if the resource is found
     * 404 if the resource is not found
     */
    public function show(string $medverifyId): JsonResponse
    {
        // default status if patient resource is not found.
        $statusCode = 404;

        // by default, pull username by medverify uuid. if not valid, then pull by their patient id.
        $fieldName = 'medverify_id';

        if(!ValidationHelper::isValidUUID($medverifyId)) {
            $fieldName = 'patient_id';
        }

        // get patient with medical records
        $patient = Patient::where($fieldName, $medverifyId)->with(
            'medicalRecords.doctor',
            'medicalRecords.prescriptions.medication',
        )->get();

        // set to 200 if results are found.
        if ($patient->count() != 0) {
            $statusCode = 200;
        }

        return response()->json(['data' => $patient], $statusCode);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PatientRecordCreateRequest $request): JsonResponse
    {
        $statusCode = 400;

        // Creates a new patient record and loads it into the database.
        $patientRecord = Patient::create($request->toArray());

        if ($patientRecord->save()) {
            // status code 201 for the resource that was createdz
            $statusCode = 201;
        }

        return response()->json([], $statusCode);
    }

    /**
     * Update the specified patient resource
     *
     * 204 when update successfuly
     * 304 when update was not completed.
     */
    public function update(PatientRecordUpdateRequest $request, string $medverifyId): JsonResponse
    {
        // default status code
        $statusCode = 304;

        $patient = Patient::where('medverify_id', $medverifyId)
            ->update($request->toArray());

        if ($patient) {
            $statusCode = 204;
        }

        return response()->json([], $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * Returns 204 if resource has been deleted
     * Returns 410 if resource no longer exists
     */
    public function destroy(string $medverifyId): JsonResponse
    {
        //
        $patient = Patient::where('medverify_id', $medverifyId)->first();

        if($patient) {
            $patient->delete();

            // return resource deleted status
            return response()->json(
                [], 204
            );
        }

        return response()->json([], 410);
    }
}
