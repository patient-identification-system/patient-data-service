<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * CREATE TABLE Prescriptions (
         * prescription_id INT PRIMARY KEY,
         * medical_record_id INT,
         * medication_id INT,
         * FOREIGN KEY (medical_record_id) REFERENCES medical_records(medical_record_id),
         * FOREIGN KEY (medication_id) REFERENCES medications(medication_id)
     * );
     */
    public function up(): void
    {
        Schema::create('prescriptions', function (Blueprint $table) {
            $table->bigIncrements('prescription_id');
            $table->unsignedBigInteger('medical_record_id');
            $table->unsignedBigInteger('medication_id');
            $table->timestamps();

            $table->foreign('medical_record_id')->references('medical_record_id')->on('medical_records')->cascadeOnDelete();
            $table->foreign('medication_id')->references('medication_id')->on('medications')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('prescriptions');
    }
};
