<?php

namespace App\Providers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * Force the usage of ipv4 to decrease timeouts
     */
    public function boot(): void
    {
        //
        Http::macro('authService', function(string $token) {
             return Http::withOptions([
                 'force_ip_resolve' => 'v4'
             ])->withHeaders([
                 "Authorization" => "Bearer $token"
             ])->baseUrl(env('AUTH_SERVICE_URL'));
        });
    }
}
