<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Medication extends Model
{
    use HasFactory;

    protected $fillable = [
        'medication_name',
        'dosage',
    ];

    protected $primaryKey = 'medication_id';

    public function prescriptions(): HasMany
    {
        return $this->hasMany(Prescription::class, 'medication_id');
    }
}
