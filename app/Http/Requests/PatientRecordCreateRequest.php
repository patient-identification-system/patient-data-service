<?php

namespace App\Http\Requests;

use App\Validation\GenderRule;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class PatientRecordCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'dob' => 'required|date',
            'gender' => ['required', new GenderRule()],
            'contact_number' => 'required|max:25',
            'address' => 'required|max:255'
        ];
    }
}
