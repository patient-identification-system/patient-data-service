<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * CREATE TABLE patients (
         * patient_id INT PRIMARY KEY,
         * medverify_id VARCHAR(50),
         * first_name VARCHAR(50),
         * last_name VARCHAR(50),
         * dob DATE,
         * gender VARCHAR(10),
         * contact_number VARCHAR(15),
         * address VARCHAR(100)
     * );
     */
    public function up(): void
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->bigIncrements('patient_id');
            $table->string('medverify_id', 50)->default(Str::uuid());
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->date('dob');
            $table->string('gender', 10);
            $table->string('contact_number', 25);
            $table->string('address', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('patients');
    }
};
