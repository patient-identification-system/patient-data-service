<?php

namespace App\Validation;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class GenderRule implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $genders = ['m', 'f'];

        if(!in_array($value, $genders)) {
            $fail("The :attribute must be either m or f.");
        }
    }
}
