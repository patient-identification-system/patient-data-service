<?php

namespace Database\Factories;

use App\Models\Doctor;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DoctorFactory extends Factory
{
    protected $model = Doctor::class;

    /**
     * definition for our fake data
     *
     * @return array|mixed[]
     */
    public function definition(): array
    {
        $gender = FakeDataFactoryHelpers::getRandomGender();

        return [
            'first_name' => fake()->firstName($gender),
            'last_name' => fake()->lastName(),
            'specialty' => FakeDataFactoryHelpers::getRandomSpecialty(),
        ];
    }
}
