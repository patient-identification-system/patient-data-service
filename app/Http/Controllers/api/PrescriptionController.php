<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PrescriptionCreateRequest;
use App\Http\Requests\PrescriptionUpdateRequest;
use App\Models\MedicalRecord;
use App\Models\Medication;
use App\Models\Patient;
use App\Models\Prescription;
use Illuminate\Http\Request;

class PrescriptionController extends Controller
{
    /**
     * Get a specific medical record
     */
    public function show(string $id)
    {
        $statusCode = 404;
        //
        $prescription = Prescription::where('prescription_id', $id)->with('medication')->get();

        // set to 200 if results are found.
        if ($prescription->count() != 0) {
            $statusCode = 200;
        }

        return response()->json(['data' => $prescription], $statusCode);
    }

    /**
     * Create a new medical record tied to a patient and doctor
     */
    public function store(PrescriptionCreateRequest $request)
    {
        //
        $statusCode = 400;

        // creates a medication.
        $medication = Medication::create(
            [
                'medication_name' => $request->medication_name,
                'dosage' => $request->dosage,
            ]
        );

        // creates a prescription and assigns the medication.
        $medication->prescriptions()->create(
            [
                'medical_record_id' => $request->medical_record_id,
            ]
        );

        if ($medication->save()) {
            // status code 201 for the resource that was created
            $statusCode = 201;
        }

        return response()->json([], $statusCode);
    }

    /**
     * Update an existing medical record
     */
    public function update(PrescriptionUpdateRequest $request, string $id)
    {
        //
        
    }

    /**
     * Delete a medical record
     */
    public function destroy(string $id)
    {
        //
        $prescription = Prescription::where('prescription_id', $id)->first();

        if($prescription) {
            $prescription->delete();

            // return resource deleted status
            return response()->json(
                [], 204
            );
        }

        return response()->json([], 410);
    }
}
