<?php

namespace Database\Seeders;

use Database\Factories\FakeDataFactoryHelpers;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class MedicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * random generator for medications.
     */
    public function run(): void
    {
        //
        $medications = FakeDataFactoryHelpers::getMedicationNames();

        foreach ($medications as $medication) {
            DB::table('medications')->insert([
                'medication_name' => $medication,
                'dosage' => rand(1, 400) . "mg",
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
