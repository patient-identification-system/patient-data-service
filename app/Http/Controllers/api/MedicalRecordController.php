<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\MedicalRecordCreateRequest;
use App\Http\Requests\MedicalRecordUpdateRequest;
use App\Models\MedicalRecord;
use App\Models\Patient;
use Illuminate\Http\Request;

class MedicalRecordController extends Controller
{
    /**
     * Display the specified resource.
     */
    public function show(string $medicalRecordId)
    {
        $statusCode = 404;
        //
        $medicalRecord = MedicalRecord::where('medical_record_id', $medicalRecordId)->get();

        // set to 200 if results are found.
        if ($medicalRecord->count() != 0) {
            $statusCode = 200;
        }

        return response()->json(['data' => $medicalRecord], $statusCode);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MedicalRecordCreateRequest $request)
    {
        $statusCode = 400;

        // Creates a new patient record and loads it into the database.
        $medicalRecord = MedicalRecord::create($request->toArray());

        if ($medicalRecord->save()) {
            // status code 201 for the resource that was createdz
            $statusCode = 201;
        }

        return response()->json([], $statusCode);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MedicalRecordUpdateRequest $request, string $id)
    {
        // default status code
        $statusCode = 304;

        $patient = MedicalRecord::where('medical_record_id', $id)
            ->update($request->toArray());

        if ($patient) {
            $statusCode = 204;
        }

        return response()->json([], $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $medicalRecord = MedicalRecord::where('medical_record_id', $id)->first();

        if($medicalRecord) {
            $medicalRecord->delete();

            // return resource deleted status
            return response()->json(
                [], 204
            );
        }

        return response()->json([], 410);
    }
}
