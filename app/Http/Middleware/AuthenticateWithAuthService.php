<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;

class AuthenticateWithAuthService
{
    /**
     * Middleware that connects to auth service to check if tokens are valid.
     *
     * @param Closure(Request): (Response) $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $authToken = $request->bearerToken() ?? "";

        $httpResponse = Http::authService($authToken)->get('/api/validate-token');

        if($httpResponse->successful()) {
            return $next($request);
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }
}
