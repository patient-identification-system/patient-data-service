<?php

namespace App\Helpers;

class ValidationHelper
{

    /**
     * Used regex to validate UUIDs from here:
     * https://stackoverflow.com/questions/7905929/how-to-test-valid-uuid-guid
     *
     * @param $uuid
     * @return bool
     */
    public static function isValidUUID($uuid): bool
    {
        $pattern = '/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i';
        return preg_match($pattern, $uuid) === 1;
    }

}
