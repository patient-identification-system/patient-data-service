<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * CREATE TABLE medical_records (
         * record_id INT PRIMARY KEY,
         * patient_id INT,
         * record_date DATE,
         * diagnosis VARCHAR(255),
         * treatment VARCHAR(255),
         * doctor_id INT,
         * FOREIGN KEY (patient_id) REFERENCES patients(patient_id),
         * FOREIGN KEY (doctor_id) REFERENCES doctors(doctor_id)
     * );
     */
    public function up(): void
    {
        Schema::create('medical_records', function (Blueprint $table) {
            $table->bigIncrements('medical_record_id');
            $table->unsignedBigInteger('patient_id');
            $table->date('record_date');
            $table->string('diagnosis', 255);
            $table->string('treatment', 255);
            $table->unsignedBigInteger('doctor_id');
            $table->timestamps();

            $table->foreign('patient_id')->references('patient_id')->on('patients')->cascadeOnDelete();
            $table->foreign('doctor_id')->references('doctor_id')->on('doctors');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('medical_records');
    }
};
