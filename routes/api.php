<?php

use App\Http\Controllers\api\MedicalRecordController;
use App\Http\Controllers\api\PatientInformationController;
use App\Http\Controllers\api\PrescriptionController;
use App\Http\Controllers\api\StatusController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/status', [StatusController::class, 'index']);

// routes here are protected by auth service middleware
Route::middleware(['authWithAuthService'])->group(function() {

    // patient resource route
    Route::resource('patient', PatientInformationController::class);

    // patient medical record/history route
    Route::resource('history', MedicalRecordController::class);

    // patient prescription route
    Route::resource('prescription', PrescriptionController::class);
});
