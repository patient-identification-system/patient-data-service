<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;

class StatusController extends Controller
{
    /**
     * returns a status page and the name of what this service does
     *
     */
    public function index()
    {
        // build our dto
        $status = [
            'status' => 200,
            'description' => 'This is the patient data microservice. This service stores, updates, retrieves, and deletes patient data.'
        ];

        return response($status);
    }
}
