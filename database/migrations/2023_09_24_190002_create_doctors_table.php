<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * CREATE TABLE doctors (
         * doctor_id INT PRIMARY KEY,
         * first_name VARCHAR(50),
         * last_name VARCHAR(50),
         * specialty VARCHAR(50)
     * );
     */
    public function up(): void
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->bigIncrements('doctor_id');
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('specialty', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('doctors');
    }
};
