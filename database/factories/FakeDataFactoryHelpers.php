<?php

namespace Database\Factories;

class FakeDataFactoryHelpers
{
    public static function getDiagnosesAndTreatments(): array
    {
        return [
            "Common Cold" => "Rest, hydration, over-the-counter cold medications",
            "Influenza (Flu)" => "Rest, hydration, antiviral medications, pain relievers",
            "Strep Throat" => "Antibiotics, pain relievers, throat lozenges, rest",
            "Sinusitis" => "Nasal decongestants, saline nasal washes, antibiotics (if bacterial)",
            "Bronchitis" => "Rest, hydration, cough suppressants, bronchodilators",
            "Pneumonia" => "Antibiotics, rest, hydration, oxygen therapy",
            "Asthma" => "Bronchodilators, inhaled corticosteroids, leukotriene modifiers",
            "Allergies" => "Antihistamines, decongestants, allergy shots (immunotherapy)",
            "Gastroenteritis" => "Rest, hydration, antiemetics (to control vomiting), bland diet",
            "Appendicitis" => "Surgery (appendectomy), antibiotics (if mild infection)",
            "Kidney Stones" => "Pain relievers, hydration, medications to help pass the stones",
            "Urinary Tract Infection (UTI)" => "Antibiotics, increased hydration, pain relievers",
            "Diabetes" => "Insulin therapy, oral medications, blood sugar monitoring, lifestyle changes",
            "Hypertension" => "Antihypertensive medications, lifestyle changes (diet, exercise)",
            "Migraine" => "Pain relievers, triptans, preventive medications, lifestyle changes",
            "Concussion" => "Rest, cognitive and physical rest, pain relievers",
            "Sprained Ankle" => "Rest, ice, compression, elevation (RICE), pain relievers",
            "Fractured Bone" => "Immobilization (cast, splint), pain relievers, possible surgery",
            "Arthritis" => "Pain relievers, anti-inflammatory medications, physical therapy, lifestyle changes",
            "Rheumatoid Arthritis" => "Disease-modifying antirheumatic drugs (DMARDs), biologics, physical therapy",
            "Osteoarthritis" => "Pain relievers, physical therapy, weight management, lifestyle changes",
            "Lupus" => "Corticosteroids, immunosuppressive drugs, lifestyle changes",
            "Multiple Sclerosis (MS)" => "Disease-modifying therapies, physical therapy, lifestyle changes",
            "Parkinson's Disease" => "Levodopa, dopamine agonists, MAO-B inhibitors, physical therapy",
            "Alzheimer's Disease" => "Cholinesterase inhibitors, NMDA receptor antagonists, lifestyle changes",
            "Depression" => "Antidepressants, psychotherapy, electroconvulsive therapy (ECT), lifestyle changes",
            "Anxiety" => "Antidepressants, anti-anxiety medications, psychotherapy, lifestyle changes",
            "Bipolar Disorder" => "Mood stabilizers, antipsychotics, psychotherapy, lifestyle changes",
            "Schizophrenia" => "Antipsychotic medications, psychotherapy, social support, lifestyle changes",
            "Obsessive-Compulsive Disorder (OCD)" => "Selective serotonin reuptake inhibitors (SSRIs), cognitive-behavioral therapy (CBT)",
            "Attention Deficit Hyperactivity Disorder (ADHD)" => "Stimulant medications, non-stimulant medications, behavioral therapy",
            "Eating Disorders" => "Psychotherapy, nutritional counseling, medications (in some cases)",
            "Obesity" => "Diet and exercise counseling, medications, bariatric surgery",
            "Chronic Fatigue Syndrome" => "Symptomatic treatment, cognitive-behavioral therapy (CBT), graded exercise therapy",
            "Fibromyalgia" => "Pain relievers, antidepressants, physical therapy, lifestyle changes",
            "Cancer" => "Surgery, chemotherapy, radiation therapy, immunotherapy, targeted therapy",
            "Heart Attack" => "Aspirin, thrombolytic therapy, angioplasty, bypass surgery, lifestyle changes",
            "Stroke" => "Clot-busting medications, antiplatelet drugs, rehabilitation therapy, lifestyle changes",
            "Chronic Obstructive Pulmonary Disease (COPD)" => "Bronchodilators, inhaled steroids, oxygen therapy, pulmonary rehabilitation",
            "Atrial Fibrillation" => "Medications (antiarrhythmics, anticoagulants), cardioversion, ablation",
            "Peptic Ulcer Disease" => "Antibiotics (if caused by H. pylori), acid reducers, lifestyle changes",
            "Gastroesophageal Reflux Disease (GERD)" => "Antacids, proton pump inhibitors, lifestyle changes",
            "Irritable Bowel Syndrome (IBS)" => "Dietary changes, fiber supplements, antispasmodic medications",
            "Crohn's Disease" => "Anti-inflammatory medications, immunosuppressants, biologics, dietary changes",
            "Ulcerative Colitis" => "Anti-inflammatory medications, immunosuppressants, biologics, dietary changes",
            "Diverticulitis" => "Antibiotics, clear liquid diet, transition to a high-fiber diet",
            "Endometriosis" => "Pain relievers, hormone therapy, laparoscopic surgery",
            "Polycystic Ovary Syndrome (PCOS)" => "Birth control pills, metformin, lifestyle changes",
        ];
    }

    public static function getMedicationNames(): array
    {
        return [
            'Aspirin',
            'Ibuprofen',
            'Paracetamol',
            'Amoxicillin',
            'Ciprofloxacin',
            'Lisinopril',
            'Simvastatin',
            'Metformin',
            'Omeprazole',
            'Atorvastatin',
            'Levothyroxine',
            'Losartan',
            'Gabapentin',
            'Hydrochlorothiazide',
            'Metoprolol',
            'Albuterol',
            'Escitalopram',
            'Sertraline',
            'Warfarin',
            'Furosemide'
        ];
    }

    /**
     * Random number generator that returns male or female for patients every
     * reseed
     *
     * @return string
     */
    public static function getRandomSpecialty(): string
    {
        $doctorSpecialties = [
            'Cardiologist',
            'Dermatologist',
            'Endocrinologist',
            'Gastroenterologist',
            'Hematologist',
            'Neurologist',
            'Oncologist',
            'Orthopedic Surgeon',
            'Pediatrician',
            'Psychiatrist',
            'Radiologist',
            'Rheumatologist',
            'Urologist',
            'Nephrologist',
            'Gynecologist',
            'Ophthalmologist',
            'Otolaryngologist',
            'Pulmonologist',
            'Allergist',
            'Geriatrician'
        ];

        $randomIndex = rand(0, 19);

        return $doctorSpecialties[$randomIndex];
    }

    /**
     * Random number generator that returns male or female for patients every
     * reseed
     *
     * @return string
     */
    public static function getRandomGender(): string {
        $genders = array("male", "female");
        $randomIndex = rand(0, 1);
        return $genders[$randomIndex];
    }
}
