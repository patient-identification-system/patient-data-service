<?php

namespace Database\Factories;

use App\Models\Patient;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PatientFactory extends Factory
{
    protected $model = Patient::class;

    /**
     * definition for our fake data
     *
     * @return array|mixed[]
     */
    public function definition(): array
    {
        $gender = FakeDataFactoryHelpers::getRandomGender();

        return [
            'medverify_id' => Str::uuid(),
            'first_name' => fake()->firstName($gender),
            'last_name' => fake()->lastName(),
            'dob' => fake()->date(),
            'gender' => substr($gender, 0, 1), // get m or f from "male" or "female"
            'contact_number' => fake()->phoneNumber(),
            'address' => fake()->address(),
        ];
    }
}
