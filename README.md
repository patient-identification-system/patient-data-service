# Patient Data Service

The Patient Data Service (PDS) is in charge of getting, and managing patient data information, and has API endpoints for different operations.

Dependencies:
- PHP 8.1
- Laravel 10
- PHP Extensions
